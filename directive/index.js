'use strict';
var util = require('util');
var ScriptBase = require('../script-base.js');
var _ = require('underscore.string');


var Generator = module.exports = function Generator() {
  ScriptBase.apply(this, arguments);
};

util.inherits(Generator, ScriptBase);

Generator.prototype.createDirectiveFiles = function createDirectiveFiles() {
  this.generateSourceAndTest(
    'directive',
    'spec/directive',
    ('../common/directives/' + _.dasherize(this.name)),
    ('./common/directives/' + _.dasherize(this.name))
  );
};
