'use strict';
var util = require('util');
var _ = require('underscore.string');
var ScriptBase = require('../script-base.js');


var Generator = module.exports = function Generator() {
  ScriptBase.apply(this, arguments);
};

util.inherits(Generator, ScriptBase);

Generator.prototype.createServiceFiles = function createServiceFiles() {
  this.generateSourceAndTest(
    'service/factory',
    'spec/service',
    ('../common/factories/' + _.dasherize(this.name)),
    ('./common/factories/' + _.dasherize(this.name))
  );
};
