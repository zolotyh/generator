'use strict';
var util = require('util');
var ScriptBase = require('../script-base.js');
var _ = require('underscore.string');


var Generator = module.exports = function Generator() {
  ScriptBase.apply(this, arguments);
};

util.inherits(Generator, ScriptBase);

Generator.prototype.createFilterFiles = function createFilterFiles() {
  this.generateSourceAndTest(
    'filter',
    'spec/filter',
    ('../common/filters/' + _.dasherize(this.name)),
    ('./common/filters/' + _.dasherize(this.name))
  );
};
