'use strict';
var util = require('util');
var _ = require('underscore.string');
var ScriptBase = require('../script-base.js');


var Generator = module.exports = function Generator() {
  ScriptBase.apply(this, arguments);
};

util.inherits(Generator, ScriptBase);

Generator.prototype.createServiceFiles = function createServiceFiles() {
  this.generateSourceAndTest(
    'service/provider',
    'spec/service',
    ('../common/providers/' + _.dasherize(this.name)),
    ('./common/providers/' + _.dasherize(this.name))
  );
};
